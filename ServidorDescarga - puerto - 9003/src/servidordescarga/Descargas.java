/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidordescarga;

/**
 *
 * @author Macedo
 */
public class Descargas {
    
    private String nombre;
    private String ip;
    private int puerto; 
    private String ruta;
    private int tamaño;

    /**
     * Constructor de descargas
     * @param nombre
     * @param ip
     * @param puerto
     * @param ruta
     * @param tamaño
     */
    public Descargas(String nombre, String ip, int puerto,String ruta, int tamaño) {
        this.nombre = nombre;
        this.ip = ip;
        this.puerto = puerto;
        this.ruta = ruta;
        this.tamaño = tamaño;
    }

    /**
     * Get ruta
     * @return
     */
    public String getRuta() {
        return ruta;
    }

    /**
     * Set ruta
     * @param ruta
     */
    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    /**
     * Get nombre
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Set nombre
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Get ip
     * @return
     */
    public String getIp() {
        return ip;
    }

    /**
     * Set ip
     * @param ip
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * Get ip
     * @return
     */
    public int getPuerto() {
        return puerto;
    }

    /**
     * Set puerto
     * @param puerto
     */
    public void setPuerto(int puerto) {
        this.puerto = puerto;
    }
    
    /**
     * Get puerto
     * @return
     */
    public int getTamaño() {
        return puerto;
    }

    /**
     * Set tamaño
     * @param tamaño
     */
    public void setTamaño(int tamaño) {
        this.tamaño = tamaño;
    }
    
}
