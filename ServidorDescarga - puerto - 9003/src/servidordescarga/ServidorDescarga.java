/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidordescarga;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Juan
 */
public class ServidorDescarga {

    /**
     * MAIN
     * @param args
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Pantalla pantalla = new Pantalla();
        pantalla.setVisible(true);
        //ip del servidor central y puerto del servidor central
        FuncionCliente cliente = new FuncionCliente("localhost",8181);
        
        // ip y puerto del servidor de descarga
        String respuesta = cliente.Enviar_mensaje("4:localhost:9003"); //cambie aca puerto
        
        ServidorD server = new ServidorD(9003); // cambie puerto aca
        server.start();
        
      /*  
        try{
            ServerSocket serversock = new ServerSocket(9494);
            while(true){
                Socket  socket = serversock.accept();
                new Thread(new ServidorD(socket)).start();
            }
        }catch(IOException e){}
        }
               */
    }
    
}
