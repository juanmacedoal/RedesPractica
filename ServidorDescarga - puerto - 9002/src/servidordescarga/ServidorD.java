/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidordescarga;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Juan
 */
public class ServidorD extends Thread implements Runnable {

    private ServerSocket server;
    private int puerto;
    boolean parrar;

    /**
     * Puerto de escucha
     *
     * @param PtoEscucha
     */
    public ServidorD(int PtoEscucha) {
        this.puerto = PtoEscucha;

    }

    /**
     * Detiene la conexion
     */
    @Override
    public void interrupt() {
        System.out.println("Entrada");
        parrar = false;
    }

    /**
     * Hilo abierto de conexion
     */
    @Override
    public void run() {
        try {

            server = new ServerSocket(this.puerto);
            System.out.println("Servidor de Descarga 1 Puerto: 9002");
            System.out.println("Inicio de conexion con Servidor Central \t[OK]");
            int idSession = 0;
            parrar = true;

            while (parrar) {
                Socket socket;
                socket = server.accept();
                System.out.println("Nueva Conexion : " + socket);
                ((HiloDescarga) new HiloDescarga(socket, idSession)).start();
                idSession++;
            }

        } catch (IOException ex) {
            Logger.getLogger(ServidorD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
