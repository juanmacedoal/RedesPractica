/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servidordescarga;

import java.io.*;
import java.net.*;
import java.util.logging.*;
import org.jdom2.JDOMException;

/**
 *
 * @author Macedo
 */
public class HiloDescarga extends Thread {

    private Socket socket;
    private DataOutputStream out;
    private DataInputStream dis;
    private int Sesion;

    /**
     * Recibe ip y puerto de conexion
     *
     * @param socket
     * @param id
     */
    public HiloDescarga(Socket socket, int id) {
        this.socket = socket;
        this.Sesion = id;
        try {
            out = new DataOutputStream(socket.getOutputStream());
            dis = new DataInputStream(socket.getInputStream());
        } catch (IOException ex) {
            Logger.getLogger(HiloDescarga.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Desconexion de socket
     */
    public void desconexion() {
        try {
            this.socket.close();
        } catch (IOException ex) {
            Logger.getLogger(HiloDescarga.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //RECIBE LAS PETICIONES
    /**
     *
     * @return @throws IOException
     */
    public String RecibirD() throws IOException {
        return dis.readUTF();
    }

    //RESPONDE LAS PETICIONES
    /**
     *
     * @param dato
     * @throws IOException
     */
    public void Responder(String dato) throws IOException {
        out.writeUTF(dato);
    }

    /**
     * Hilo que recibe peticiones
     */
    @Override
    public void run() {
        String peticion = "";

        System.out.println("");
        try {

            peticion = RecibirD();
            String[] opcion = peticion.toString().split(":");
            //System.out.println(opcion[0]);

            int operacion = Integer.parseInt(opcion[0]);

            switch (operacion) {
                //VERIFICA LOS SERVIDORES DE DESCARGAS ACTIVOS 
                case 1: {
                    Videos lista = new Videos();
                    int cont = 0;
                    String repuesta = "";
                    System.out.println("caso 1");
                    lista = VideoFuncionesXml.readAllObjectsFromFile2(lista);
                    System.out.println("Lista tamano" + lista.tamano_Videos());
                    while (cont < lista.tamano_Videos()) {
                        System.out.println(" Lista " + lista.buscarVideo(cont).getNombre());
                        repuesta = repuesta + lista.buscarVideo(cont).getNombre() + "#" + lista.buscarVideo(cont).getRuta() + "#" + lista.buscarVideo(cont).getIp() + "#" + lista.buscarVideo(cont).getPuerto() + "@";
                        cont++;
                    }
                    System.out.println(" Repuesta " + repuesta);
                    Responder(repuesta);

                    break;
                }
                // REGISTRAR
                case 2: {
                    String Cliente = String.valueOf(funcionesXmlDescarga.PersonasDescargando());
                    System.out.println("Clientes cantidad = " + Cliente);
                    Responder(Cliente);
                    break;
                }
                case 3: //ENVIO PARTE 1
                {
                    //System.out.println(opcion[1]);
                    String nombreArchivo = opcion[1];
                    System.out.println("Descargando Archivo " + nombreArchivo);
                    nombreArchivo = "C:\\Users\\Macedo\\Desktop\\ultimo\\Redes\\ServidorDescarga - puerto - 9002\\src\\Videos\\" + opcion[1] + "part2.zip";
                    //GUARDAMOS EL ARCHIVO
                    Socket sockete = new Socket(opcion[2], Integer.parseInt(opcion[3]));
                    File archivo = new File(nombreArchivo);

                    // TAMAÑO DEL VIDEO
                    int tamañoArchivo = (int) archivo.length();

                    DataOutputStream dosi = new DataOutputStream(sockete.getOutputStream());
                    dosi.writeUTF("1:" + opcion[1] + ":" + "2");
                    dosi.writeUTF(String.valueOf(tamañoArchivo));
                    FileInputStream fis = new FileInputStream(nombreArchivo);
                    BufferedInputStream bis = new BufferedInputStream(fis);

                    // ENVIAMOS DATOS DEL VIDEO BYTE POR BYTE
                    BufferedOutputStream bos = new BufferedOutputStream(sockete.getOutputStream());

                    //CREACION DE UN ARREGLO CON SU NOMBRE DEL VIDEO
                    byte[] buffer = new byte[tamañoArchivo];

                    //LECTURA DE ARCHIVO Y GUARDAMOS EN EL ARREGLO CREADO
                    bis.read(buffer);
                    //EVITA QUE SE GUARDEN REPETIDOS EN EL HISTORIAL
                    if (!funcionesXmlDescarga.VerificarHistorial(opcion[2], opcion[3], "des", opcion[1])) {
                        funcionesXmlDescarga.HistorialDescargas(opcion[2], opcion[3], "des", opcion[1], "0", String.valueOf(tamañoArchivo), nombreArchivo);
                    }
                    // ENVIO DE BYTE
                    for (int i = 0; i < buffer.length; i++) {
                        bos.write(buffer[i]);
                    }
                    bis.close();
                    bos.close();
                    sockete.close();
                    Responder("Descarga Realizada");
                    break;
                }
                case 4: {
                    // CASO 4 RECIBE EL TAMAÑO RESTANTE QUE FALTA POR ENVIAR
                    System.out.println("Tamaño restante case 4: ");
                    System.out.println(opcion[1]);
                    String nombreArchivo = opcion[1];
                    System.out.println("Descargando Archivo parte 2 " + nombreArchivo);
                    nombreArchivo = "C:\\Users\\Macedo\\Desktop\\ultimo\\Redes\\ServidorDescarga - puerto - 9002\\src\\Videos\\" + opcion[1];
                    //GUARDAMOS EL ARCHIVO
                    Socket sockete = new Socket(opcion[2], Integer.parseInt(opcion[3]));
                    File archivo = new File(nombreArchivo);

                    // TAMAÑO DEL VIDEO
                    int tamañoArchivo = (int) archivo.length();

                    DataOutputStream dosi = new DataOutputStream(sockete.getOutputStream());
                    dosi.writeUTF("2:" + opcion[1]);
                    dosi.writeUTF(String.valueOf(tamañoArchivo));
                    FileInputStream fis = new FileInputStream(nombreArchivo);
                    BufferedInputStream bis = new BufferedInputStream(fis);

                    // ENVIAMOS DATOS DEL VIDEO BYTE POR BYTE
                    BufferedOutputStream bos = new BufferedOutputStream(sockete.getOutputStream());

                    //CREACION DE UN ARREGLO CON SU NOMBRE DEL VIDEO
                    byte[] buffer = new byte[tamañoArchivo];

                    //LECTURA DE ARCHIVO Y GUARDAMOS EN EL ARREGLO CREADO
                    bis.read(buffer);
                    //EVITA QUE SE GUARDEN REPETIDOS EN EL HISTORIAL
                    if (!funcionesXmlDescarga.VerificarHistorial(opcion[2], opcion[3], "des", opcion[1])) {
                        funcionesXmlDescarga.HistorialDescargas(opcion[2], opcion[3], " des", opcion[1], "0", String.valueOf(tamañoArchivo), nombreArchivo);
                    }
                    // ENVIO DE BYTE DE LA PARTE SOBRANTE
                    for (int i = 0; i < buffer.length; i++) {
                        //if(i == buffer.length/3)
                        bos.write(buffer[i]);
                    }
                    bis.close();
                    bos.close();
                    sockete.close();
                    Responder("Descarga Realizada");
                    break;
                }
                case 5: {
                    // CASO 4 REVISA SI TIENE EL ARCHIVO
                    System.out.println("Comprobando que tenga el video case 4:");
                    System.out.println(opcion[1]);
                    String nombreArchivo = opcion[1];
                    
                    //EVITA QUE SE GUARDEN REPETIDOS EN EL HISTORIAL
                    if (!VideoFuncionesXml.VerificarHistorial(opcion[2], opcion[3], "des", opcion[1])) {
                        Responder("No se encuentra en el servidor");
                    }
                    
                    Responder("Se encuentra en el servidor");
                    break;
                }
                default: {
                    throw new RuntimeException("ERROR EN LA OPERACION");
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(HiloDescarga.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JDOMException ex) {
            Logger.getLogger(HiloDescarga.class.getName()).log(Level.SEVERE, null, ex);
        }

        desconexion();
    }
}
