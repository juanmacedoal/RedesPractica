/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidordescarga;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Juan
 */
public class Videos {

    private List<Video> listaVideo;

    /**
     * Crea lista de videos
     */
    public Videos() {
        this.listaVideo = new ArrayList();
    }

    /**
     * Recibe video
     *
     * @param pelicula
     */
    public void agregarVideo(Video pelicula) {
        this.listaVideo.add(pelicula);
    }

    /**
     * Cuenta video
     *
     * @param cont
     * @return
     */
    public Video buscarVideo(int cont) {
        return this.listaVideo.get(cont);
    }

    /**
     * Entrega tamaño de lista
     *
     * @return
     */
    public int tamano_Videos() {
        return this.listaVideo.size();
    }

}
