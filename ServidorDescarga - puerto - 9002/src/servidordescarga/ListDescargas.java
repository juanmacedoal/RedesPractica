/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidordescarga;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Macedo
 */
public class ListDescargas {
    
  
    private List <Descargas> listaDescargas;

    /**
     *
     */
    public ListDescargas() {
        this.listaDescargas =new ArrayList();
    }
    
    /**
     *
     * @param descarga
     */
    public void agregarDescargas(Descargas descarga )
    {
        this.listaDescargas.add(descarga);
    }
    
    /**
     *
     * @param cont
     * @return
     */
    public Descargas buscardescarga(int cont)
    {
        return this.listaDescargas.get(cont);
    }
    
    /**
     *
     * @return
     */
    public int tamano_descarga()
    {
        return this.listaDescargas.size();
    }
    
}
