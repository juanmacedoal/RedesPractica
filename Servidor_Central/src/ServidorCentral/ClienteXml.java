/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ServidorCentral;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.JDOMParseException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class ClienteXml implements Serializable {

    /**
     * Lee xml archivo
     * @param user
     * @return
     * @throws JDOMException
     * @throws IOException
     */
    public static boolean readObjectFromFile(Cliente user) throws JDOMException, IOException {
        Document doc;
        Element root, child;
        List<Element> rootChildrens;
        String nickName, password, ip, puerto;
        boolean found = false;
        int pos = 0;

        SAXBuilder builder = new SAXBuilder();

        doc = (Document) builder.build(UtilXml.USERS_XML_PATH);
        root = doc.getRootElement();
        rootChildrens = root.getChildren();
        while (!found && pos < rootChildrens.size()) {
            child = rootChildrens.get(pos);

            nickName = child.getAttributeValue(UtilXml.USER_NICKNAME_TAG);
            password = child.getAttributeValue(UtilXml.USER_PASSWORD_TAG);
            ip = child.getAttributeValue(UtilXml.USER_IP_TAG);
            puerto = child.getAttributeValue(UtilXml.USER_PUERTO_TAG);

            System.out.println(" usuario" + nickName + " usuario out " + user.getNickname());
            if (nickName != null && nickName.equals(user.getNickname())) {
                found = true;
            } else {

                if (nickName == null) {
                    System.out.println(UtilXml.ERROR_USER_NICKNAME_TAG);
                }

                if (password == null) {
                    System.out.println(UtilXml.ERROR_USER_PASSWORD_TAG);
                }

                if (ip == null) {
                    System.out.println(UtilXml.ERROR_USER_IP_TAG);
                }

                if (puerto == null) {
                    System.out.println(UtilXml.ERROR_USER_PUERTO_TAG);
                }

                pos++;
            }

        }

        return found;
    }
    /**
     * Guarda xml archivo
     * @param user
     * @return
     * @throws JDOMException
     * @throws IOException 
     */
    public static boolean saveObjectToFile(Cliente user) throws JDOMException, IOException {
        Document doc;
        Element root, newChild;

        SAXBuilder builder = new SAXBuilder();

        doc = builder.build(UtilXml.USERS_XML_PATH);
        root = doc.getRootElement();
        newChild = new Element(UtilXml.SERVIDOR_TAG);
        newChild.setAttribute(UtilXml.USER_NICKNAME_TAG, user.getNickname());
        newChild.setAttribute(UtilXml.USER_PASSWORD_TAG, user.getPassword());
        newChild.setAttribute(UtilXml.USER_IP_TAG, user.getIp());
        newChild.setAttribute(UtilXml.USER_PUERTO_TAG, String.valueOf(user.getPuerto()));
        root.addContent(newChild);
        try {
            Format format = Format.getPrettyFormat();

            /* Se genera un flujo de salida de datos XML */
            XMLOutputter out = new XMLOutputter(format);

            /* Se asocia el flujo de salida con el archivo donde se guardaran los datos */
            FileOutputStream file = new FileOutputStream(UtilXml.USERS_XML_PATH);

            /* Se manda el documento generado hacia el archivo XML */
            out.output(doc, file);

            /* Se limpia el buffer ocupado por el objeto file y se manda a cerrar el archivo */
            file.flush();
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }
    /**
     * Lee un cliente
     * @param nicknameuser
     * @param passworduser
     * @param ipuser
     * @param puertouser
     * @return
     * @throws JDOMException
     * @throws IOException 
     */
    public static Cliente readObjectFromFile3(String nicknameuser, String passworduser, String ipuser, String puertouser) throws JDOMException, IOException {
        Cliente user = null;
        Document doc;
        Element root, child, newchild;
        List<Element> rootChildrens;
        String nickName, password, ip, puerto;
        boolean found = false;
        int pos = 0;
        boolean save = false;

        SAXBuilder builder = new SAXBuilder();

        try {
            doc = builder.build(UtilXml.USERS_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size()) {
                child = rootChildrens.get(pos);

                nickName = child.getAttributeValue(UtilXml.USER_NICKNAME_TAG);
                password = child.getAttributeValue(UtilXml.USER_PASSWORD_TAG);
                ip = child.getAttributeValue(UtilXml.USER_IP_TAG);
                puerto = child.getAttributeValue(UtilXml.USER_PUERTO_TAG);

                if (nickName != null && password != null && nickName.equals(nicknameuser) && password.equals(passworduser)) {

                    child.setAttribute(UtilXml.USER_NICKNAME_TAG, nickName);
                    child.setAttribute(UtilXml.USER_PASSWORD_TAG, password);
                    child.setAttribute(UtilXml.USER_IP_TAG, ipuser);
                    child.setAttribute(UtilXml.USER_PUERTO_TAG, puertouser);

                    user = new Cliente(nickName, password, ipuser, Integer.parseInt(puertouser));
                    found = true;

                } else {

                    if (nickName == null) {
                        System.out.println(UtilXml.ERROR_USER_NICKNAME_TAG);
                    }

                    if (password == null) {
                        System.out.println(UtilXml.ERROR_USER_PASSWORD_TAG);
                    }

                    if (ip == null) {
                        System.out.println(UtilXml.ERROR_USER_IP_TAG);
                    }

                    if (puerto == null) {
                        System.out.println(UtilXml.ERROR_USER_PUERTO_TAG);
                    }

                    pos++;
                }
            }
            try {
                Format format = Format.getPrettyFormat();
                XMLOutputter out = new XMLOutputter(format);
                FileOutputStream file = new FileOutputStream(UtilXml.USERS_XML_PATH);
                out.output(doc, file);
                file.flush();
                file.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (JDOMParseException e) {
            System.out.println(UtilXml.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        } catch (JDOMException e) {
            System.out.println(UtilXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println(UtilXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return user;
    }

}
