/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ServidorCentral;

import java.io.*;
import java.io.Serializable;
import java.net.Socket;
import java.net.UnknownHostException;
import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.*;
import java.util.logging.*;
import javax.swing.*;
import java.io.*;
import java.net.*;
import java.util.logging.*;

/**
 *
 * @author Luis
 */
public class FuncionCliente implements Serializable {

    private Socket s;
    private DataOutputStream out;
    private DataInputStream in;
    private ObjectOutputStream salida;
    private ObjectInputStream entrada;

    /**
     * Constructor inicia la conexion
     *
     * @param host
     * @param puerto
     */
    public FuncionCliente(String host, int puerto) {
        try {
            System.out.println("Host: " + host);
            System.out.println("Puerto: " + puerto);

            s = new Socket(host, puerto);

            out = new DataOutputStream(s.getOutputStream());
            in = new DataInputStream(s.getInputStream());
        } catch (UnknownHostException e) {
            System.out.println("NO SE PUDO CONECTAR AL " + host + "POR EL " + puerto);
        } catch (IOException e) {
            System.out.println("Error en el IP: " + host + "Por el puerto: " + puerto);
        }
    }

    /**
     * Detiene la conexion
     */
    public void Desconectar() {
        try {
            out.close();
            in.close();
            s.close();
        } catch (UnknownHostException e) {
            System.out.println("NO SE PUDO CONECTAR ");
        } catch (IOException e) {
            System.out.println("ERROR ");
        }
    }

    /**
     * Envia con socket
     *
     * @param mensaje
     * @return
     */
    public String EnviaRespuesta(String mensaje) {

        String respuesta = "";

        try {

            out.writeUTF(mensaje);
            respuesta = in.readUTF();
            Desconectar();

            return respuesta;
        } catch (UnknownHostException e) {
            System.out.println("NO SE PUEDE CONECTAR ");
        } catch (IOException e) {
            System.out.println("ERROR ");
        }

        return respuesta;
    }

}
