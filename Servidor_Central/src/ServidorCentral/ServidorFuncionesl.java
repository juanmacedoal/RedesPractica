/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServidorCentral;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.JDOMParseException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author Juan
 */
public class ServidorFuncionesl {

    /**
     * Busca servidor en xml
     *
     * @param ips
     * @param puertos
     * @return
     * @throws JDOMException
     * @throws IOException
     */
    public static boolean BuscaServidor(String ips, String puertos) throws JDOMException, IOException {
        Document doc;
        Element root, child;
        List<Element> rootChildrens;
        String ip, puerto;
        boolean found = false;
        int pos = 0;

        SAXBuilder builder = new SAXBuilder();

        doc = (Document) builder.build(UtilXm3.USERS_XML_PATH);
        root = doc.getRootElement();
        rootChildrens = root.getChildren();
        while (!found && pos < rootChildrens.size()) {
            child = rootChildrens.get(pos);

            ip = child.getAttributeValue(UtilXm3.USER_IP_TAG);
            puerto = child.getAttributeValue(UtilXm3.USER_PUERTO_TAG);

            if (ip != null && puerto != null && puerto.equals(puertos)) {
                found = true;
            } else {
                if (ip == null) {
                    System.out.println(UtilXm3.ERROR_USER_IP_TAG);
                }

                if (puerto == null) {
                    System.out.println(UtilXm3.ERROR_USER_PUERTO_TAG);
                }

                pos++;
            }

        }

        return found;
    }

    /**
     * Verifica servidor
     *
     * @param ips
     * @param puertos
     * @throws JDOMException
     * @throws IOException
     */
    public static void VerificaServidor(String ips, String puertos) throws JDOMException, IOException {
        Cliente user = null;
        Document doc;
        Element root, child, newchild;
        List<Element> rootChildrens;
        String nickName, password, ip, puerto;
        boolean found = false;
        int pos = 0;
        boolean save = false;

        SAXBuilder builder = new SAXBuilder();

        try {
            doc = builder.build(UtilXm3.USERS_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size()) {
                child = rootChildrens.get(pos);

                ip = child.getAttributeValue(UtilXm3.USER_IP_TAG);
                puerto = child.getAttributeValue(UtilXm3.USER_PUERTO_TAG);

                if (ip != null && puerto != null && puerto.equals(puertos)) {
                    child.setAttribute(UtilXm3.USER_IP_TAG, ips);
                    child.setAttribute(UtilXm3.USER_PUERTO_TAG, puertos);
                    found = true;
                } else {
                    if (ip == null) {
                        System.out.println(UtilXm3.ERROR_USER_IP_TAG);
                    }

                    if (puerto == null) {
                        System.out.println(UtilXm3.ERROR_USER_PUERTO_TAG);
                    }

                    pos++;
                }
            }
            try {
                Format format = Format.getPrettyFormat();
                XMLOutputter out = new XMLOutputter(format);
                FileOutputStream file = new FileOutputStream(UtilXm3.USERS_XML_PATH);
                out.output(doc, file);
                file.flush();
                file.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (JDOMParseException e) {
            System.out.println(UtilXml.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        } catch (JDOMException e) {
            System.out.println(UtilXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println(UtilXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

    }

    //GUARDA LOS SERVIDORES    
    public static boolean GuardarServidores(String ips, String Puertos) throws JDOMException, IOException {
        Document doc;
        Element root, newChild;

        SAXBuilder builder = new SAXBuilder();

        doc = builder.build(UtilXm3.USERS_XML_PATH);
        root = doc.getRootElement();
        newChild = new Element(UtilXm3.SERVIDOR_TAG);
        newChild.setAttribute(UtilXm3.USER_IP_TAG, ips);
        newChild.setAttribute(UtilXm3.USER_PUERTO_TAG, Puertos);
        root.addContent(newChild);
        try {
            Format format = Format.getPrettyFormat();

            //SALIDA XML
            XMLOutputter out = new XMLOutputter(format);

            //GUARDA DATOS
            FileOutputStream file = new FileOutputStream(UtilXm3.USERS_XML_PATH);

            //ENVIA ARCHIVO
            out.output(doc, file);

            file.flush();
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    /**
     * Trae lista de servidores
     *
     * @return
     * @throws JDOMException
     * @throws IOException
     */
    public static String ListaDeServidores() throws JDOMException, IOException {
        Cliente user = null;
        Document doc;
        Element root, child, newchild;
        List<Element> rootChildrens;
        String nickName, password, ip, puerto, respuesta;
        boolean found = false;
        int pos = 0;
        boolean save = false;

        SAXBuilder builder = new SAXBuilder();
        respuesta = "";
        try {
            doc = builder.build(UtilXm3.USERS_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size()) {
                child = rootChildrens.get(pos);

                ip = child.getAttributeValue(UtilXm3.USER_IP_TAG);
                puerto = child.getAttributeValue(UtilXm3.USER_PUERTO_TAG);

                System.out.println(" usuario" + ip + " usuario out " + puerto);
                if (ip != null && puerto != null) {
                    respuesta = respuesta + ip + ":" + puerto + "/";

                    found = true;

                } else {

                    if (ip == null) {
                        System.out.println(UtilXm3.ERROR_USER_IP_TAG);
                    }

                    if (puerto == null) {
                        System.out.println(UtilXm3.ERROR_USER_PUERTO_TAG);
                    }

                    pos++;
                }
            }
            try {
                Format format = Format.getPrettyFormat();
                XMLOutputter out = new XMLOutputter(format);
                FileOutputStream file = new FileOutputStream(UtilXm3.USERS_XML_PATH);
                out.output(doc, file);
                file.flush();
                file.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (JDOMParseException e) {
            System.out.println(UtilXml.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        } catch (JDOMException e) {
            System.out.println(UtilXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println(UtilXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return respuesta;
    }

    /**
     * Muestra lista
     *
     * @return
     * @throws JDOMException
     * @throws IOException
     */
    public static ListaServidor ListaDeServidores_obj() throws JDOMException, IOException {
        Cliente user = null;
        Document doc;
        Element root, child, newchild;
        List<Element> rootChildrens;
        String nickName, password, ip, puerto, respuesta;
        boolean found = false;
        int pos = 0;
        boolean save = false;
        ListaServidor lista;
        lista = new ListaServidor();
        SAXBuilder builder = new SAXBuilder();
        respuesta = "";
        try {
            doc = builder.build(UtilXm3.USERS_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (pos < rootChildrens.size()) {
                child = rootChildrens.get(pos);

                ip = child.getAttributeValue(UtilXm3.USER_IP_TAG);
                puerto = child.getAttributeValue(UtilXm3.USER_PUERTO_TAG);

                System.out.println(" IP de Usuario: " + ip + " Puerto: " + puerto);
                if (ip != null && puerto != null) {
                    Servi servidor = new Servi(ip, puerto, 0);
                    lista.AgregarLista(servidor);
                } else {

                    if (ip == null) {
                        System.out.println(UtilXm3.ERROR_USER_IP_TAG);
                    }

                    if (puerto == null) {
                        System.out.println(UtilXm3.ERROR_USER_PUERTO_TAG);
                    }

                }
                pos++;
            }
            try {
                Format format = Format.getPrettyFormat();
                XMLOutputter out = new XMLOutputter(format);
                FileOutputStream file = new FileOutputStream(UtilXm3.USERS_XML_PATH);
                out.output(doc, file);
                file.flush();
                file.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (JDOMParseException e) {
            System.out.println(UtilXml.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        } catch (JDOMException e) {
            System.out.println(UtilXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println(UtilXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return lista;
    }
}
