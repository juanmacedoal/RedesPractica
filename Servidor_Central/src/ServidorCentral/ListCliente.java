/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServidorCentral;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Luis
 */
public class ListCliente {
    
    private List <Cliente> ListaCliente;
    
    /**
     * Crea una lista
     */
    public ListCliente() {
        this.ListaCliente = new ArrayList();
    }
    /**
     * Recibe una lista usuario
     * @param user 
     */
    public void  AgregarLista(Cliente user)
    {
        this.ListaCliente.add(user);
    }
    /**
     * Recibe otra por contador
     * @param cont
     * @return 
     */
    public Cliente GetListaCliente(int cont)
    {
        return this.ListaCliente.get(cont);
    }
    /**
     * Devuelve lista
     * @param user
     * @return 
     */
    public boolean BooleanListaCliente(Cliente user)
    {
        return this.BooleanListaCliente(user);
    }
       
    
}
