/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ServidorCentral;

/**
 *
 * @author Luis
 */
import java.io.*;
import java.net.*;
import java.util.logging.*;
import javax.swing.JOptionPane;
import org.jdom2.JDOMException;

public class HiloCentral extends Thread {

    private Socket socket;
    private DataOutputStream out;
    private DataInputStream dis;
    private int sesion, clientes;

    /**
     * Controlador que inicia la conexion
     *
     * @param socket
     * @param id
     */
    public HiloCentral(Socket socket, int id) {
        this.socket = socket;
        this.sesion = id;
        try {
            out = new DataOutputStream(socket.getOutputStream());
            dis = new DataInputStream(socket.getInputStream());
        } catch (IOException ex) {
            Logger.getLogger(HiloCentral.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Cierra la conexion
     */
    public void desconexion() {
        try {
            this.socket.close();
        } catch (IOException ex) {
            Logger.getLogger(HiloCentral.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //RECIBE PETICIONES DE LLEGADA
    public String RecibirD() throws IOException {
        return dis.readUTF();
    }

    //RESPONDE PETICIONES
    public void Responder(String dato) throws IOException {
        out.writeUTF(dato);
    }

    /**
     * Corre constantemente recibiendo peticiones
     */
    public void run() {

        String peticion = "";
        try {
            peticion = RecibirD();
            String[] opcion = peticion.toString().split(":");
            System.out.println("Opcion elegida: " + opcion[0]);

            int operacion = Integer.parseInt(opcion[0]);

            switch (operacion) {
                case 1: //RELACIONADO CON CLIENTE
                {
                    Cliente client = null;
                    System.out.println("USUARIO...  " + opcion[1]);

                    //VERIFICA LA EXISTENCIA DEL USUARIO
                    client = ClienteXml.readObjectFromFile3(opcion[1], opcion[2], opcion[3], opcion[4]);

                    if (client != null) {
                        Responder("Bienvenido");
                    } else {
                        Responder("El usuario no se encuentra registrado");
                    }
                    break;
                }
                //REGISTRAR LOS USUARIOS
                case 2: {
                    System.out.println("USUARIO   " + opcion[1]);
                    System.out.println("CLAVE  " + opcion[2]);

                    //VALIDACION DE EXISTENCIA DE USUARIO 
                    Cliente user = new Cliente(opcion[1], opcion[2], opcion[3], Integer.parseInt(opcion[4]));

                    if (ClienteXml.readObjectFromFile(user)) {
                        //RESPUESTA DEL SISTEMA 
                        Responder("NOMBRE DE USUARIO EXISTENTE");
                    } else {
                        ClienteXml.saveObjectToFile(user);
                        Responder("REGISTRADO SATISFACTORIAMENTE");
                    }
                    break;
                }
                //CARGO LA LISTA DE VIDEOS
                case 3: {

                    ListaVideos lista = new ListaVideos();
                    int cont = 0;
                    String repuesta = "";

                    System.out.println("Lista de Videos");
                    lista = FuncionesXml.readAllObjectsFromFile2(lista);

                    while (cont < lista.tamano_video()) {
                        System.out.println((1 + cont) + ":" + lista.buscarvideo(cont).getNombre());
                        repuesta = repuesta + lista.buscarvideo(cont).getNombre() + "/";
                        cont++;
                    }
                    //System.out.println("Respuesta de lista: " + repuesta);
                    Responder(repuesta);

                    break;
                }
                case 4: {
                    //REGISTRO DE SERVIDORES DE DESCARGA 
                    System.out.println("Puerto del servidor --> " + opcion[2]);
                    if (ServidorFuncionesl.BuscaServidor(opcion[1], opcion[2])) {
                        ServidorFuncionesl.VerificaServidor(opcion[1], opcion[2]);
                    } else {
                        ServidorFuncionesl.GuardarServidores(opcion[1], opcion[2]);
                    }
                    Responder("Conectado al servidor");
                    break;
                }
                //DESCARGA
                case 5: {
                    System.out.print("\nDescargas reales: " + funcionesXmlCola.PersonasDescargando() + "\n");
                    
                    //CONSULTA QUE ESTE EN LOS 3 SERVIDORES
                    String server1, server2, server3;
                    FuncionCliente clientes = new FuncionCliente("localhost", 9001);
                    server1 = clientes.EnviaRespuesta("5:" + opcion[1] + ":" + opcion[2] + ":" + opcion[3]);
                    clientes = new FuncionCliente("localhost", 9002);
                    server2 = clientes.EnviaRespuesta("5:" + opcion[1] + ":" + opcion[2] + ":" + opcion[3]);
                    clientes = new FuncionCliente("localhost", 9003);
                    server3 = clientes.EnviaRespuesta("5:" + opcion[1] + ":" + opcion[2] + ":" + opcion[3]);
                    //System.out.print(server1 + server2 + server3);
                    if (server1.equals("Se encuentra en el servidor")
                            && server2.equals("Se encuentra en el servidor")
                            && server3.equals("Se encuentra en el servidor")) {
                        if (funcionesXmlCola.PersonasDescargando() < 5) {
                            System.out.print("\nDetalle seguido de opcion elegida: " + opcion[1] + "\n");
                            
                            if (!funcionesXmlDescarga.VerificarHistorial(opcion[2], opcion[3], "des", opcion[1])) {
                                funcionesXmlDescarga.HistorialDescargas(opcion[2], opcion[3], "des", opcion[1]);
                                funcionesXmlCola.HistorialDescargas(opcion[2], opcion[3], "des", opcion[1]);
                            }

                            System.out.println("\nCantidad de descargas: "
                                    + funcionesXmlDescarga.PersonasDescargando() + "\n");

                            System.out.println("Descarga del usuario: " + opcion[2] + ", Puerto: " + opcion[3] + " del video: " + opcion[1] + ":" + opcion[3]);
                            
                            clientes = new FuncionCliente("localhost", 9001);
                            String res = clientes.EnviaRespuesta("3:" + opcion[1] + ":" + opcion[2] + ":" + opcion[3]);
                            Responder(res);
                        } else {
                            Responder("Servidores ocupados");
                        }
                    } else {
                        if (server1.equals("No se encuentra en el servidor")) {
                            System.out.println("Un servidor 1 no lo tiene");
                            Responder("Un servidor 1 no lo tiene");
                        }
                        if (server2.equals("No se encuentra en el servidor")) {
                            System.out.println("Un servidor 2 no lo tiene");                           
                            Responder("Un servidor 2 no lo tiene");
                        }
                        if (server3.equals("No se encuentra en el servidor")) {
                            System.out.println("Un servidor 3 no lo tiene");                           
                            Responder("Un servidor 3 no lo tiene");
                        }
                        if (server1.equals("No se encuentra en el servidor")
                                && server2.equals("No se encuentra en el servidor")) {
                            System.out.println("Un servidor 1 y 2 no lo tiene");
                            Responder("Un servidor 1 y 2 no lo tienen");
                        }
                        if (server1.equals("No se encuentra en el servidor")
                                && server3.equals("No se encuentra en el servidor")) {
                            System.out.println("Un servidor 1 y 3 no lo tiene");
                            Responder("Un servidor 1 y 3 no lo tienen");
                        }
                        if (server3.equals("No se encuentra en el servidor")
                                && server2.equals("No se encuentra en el servidor")) {
                            System.out.println("Un servidor 2 y 3 no lo tiene");
                            Responder("Un servidor 2 y 3 no lo tienen");
                        }

                    }
                    break;

                }
                case 6: {
                    if (funcionesXmlCola.PersonasDescargando() < 5) {
                        System.out.print("\nDetalle seguido de opcion elegida: " + opcion[1] + "\n");
                        /* ListaServidor lista = new ListaServidor();
                         lista = ServidorFuncionesl.ListaDeServidores_obj();

                         System.out.println("Cantidad de Clientes en servidores:" + lista.size() + "\n");
                         int cont = 0;
                         int cantidad_Cliente;

                         while (cont < lista.size()) //GUARDA EN LISTA SERVIDOR LA CANTIDAD DE CLIENTES POR SERVIDOR
                         {
                         System.out.println("IP: " + lista.GetServidor(cont).getIp() + "\nPuerto: " + lista.GetServidor(cont).getPuerto());
                         FuncionCliente cliente = new FuncionCliente(lista.GetServidor(cont).getIp(), Integer.parseInt(lista.GetServidor(cont).getPuerto()));
                         cantidad_Cliente = Integer.parseInt(cliente.EnviaRespuesta("2:"));
                         System.out.println("Cantidad clientes " + cantidad_Cliente);
                         lista.GetServidor(cont).setCantidad_cliente(cantidad_Cliente);
                         cont++;
                         }

                         cont = 1;
                         int menor = 0;
                         int puerto;
                         String ipserver;
                         menor = lista.GetServidor(0).getCantidad_cliente();
                         puerto = Integer.parseInt(lista.GetServidor(0).getPuerto());
                         ipserver = lista.GetServidor(0).getIp();

                         while (cont < lista.size()) //REVISA CUAL SERVIDOR TIENE MENOS CLIENTES
                         {
                         if (menor > lista.GetServidor(cont).getCantidad_cliente()) {
                         menor = lista.GetServidor(cont).getCantidad_cliente();
                         puerto = Integer.parseInt(lista.GetServidor(cont).getPuerto());
                         ipserver = lista.GetServidor(cont).getIp();
                         }
                         cont++;
                         }*/
                        if (!funcionesXmlDescarga.VerificarHistorial(opcion[2], opcion[3], "des", opcion[1])) {
                            funcionesXmlDescarga.HistorialDescargas(opcion[2], opcion[3], "des", opcion[1]);
                        }

                        System.out.println("Descarga del usuario: " + opcion[2] + ", Puerto: " + opcion[3] + " del video: " + opcion[1] + ":" + opcion[3]);

                        //ENVIA EL CLIENTE AL SERVIDOR DE MENOR DESCARGAS
                        FuncionCliente clientes = new FuncionCliente("localhost", 9002);
                        String res = clientes.EnviaRespuesta("3:" + opcion[1] + ":" + opcion[2] + ":" + opcion[3]);
                        Responder(res);
                    } else {
                        Responder("Servidores ocupados");
                    }
                    break;

                }
                case 7: {
                    System.out.print("\nDescargas reales: " + funcionesXmlCola.PersonasDescargando() + "\n");
                    if (funcionesXmlCola.PersonasDescargando() < 5) {
                        System.out.print("\nDetalle seguido de opcion elegida: " + opcion[1] + "\n");
                        /*ListaServidor lista = new ListaServidor();
                         lista = ServidorFuncionesl.ListaDeServidores_obj();

                         System.out.println("Cantidad de Clientes en servidores:" + lista.size() + "\n");
                         int cont = 0;
                         int cantidad_Cliente;

                         while (cont < lista.size()) //GUARDA EN LISTA SERVIDOR LA CANTIDAD DE CLIENTES POR SERVIDOR
                         {
                         System.out.println("IP: " + lista.GetServidor(cont).getIp() + "\nPuerto: " + lista.GetServidor(cont).getPuerto());
                         FuncionCliente cliente = new FuncionCliente(lista.GetServidor(cont).getIp(), Integer.parseInt(lista.GetServidor(cont).getPuerto()));
                         cantidad_Cliente = Integer.parseInt(cliente.EnviaRespuesta("2:"));
                         System.out.println("Cantidad clientes " + cantidad_Cliente);
                         lista.GetServidor(cont).setCantidad_cliente(cantidad_Cliente);
                         cont++;
                         }

                         cont = 1;
                         int menor = 0;
                         int puerto;
                         String ipserver;
                         menor = lista.GetServidor(0).getCantidad_cliente();
                         puerto = Integer.parseInt(lista.GetServidor(0).getPuerto());
                         ipserver = lista.GetServidor(0).getIp();

                         while (cont < lista.size()) //REVISA CUAL SERVIDOR TIENE MENOS CLIENTES
                         {
                         if (menor > lista.GetServidor(cont).getCantidad_cliente()) {
                         menor = lista.GetServidor(cont).getCantidad_cliente();
                         puerto = Integer.parseInt(lista.GetServidor(cont).getPuerto());
                         ipserver = lista.GetServidor(cont).getIp();
                         }
                         cont++;
                         }*/
                        if (!funcionesXmlDescarga.VerificarHistorial(opcion[2], opcion[3], "des", opcion[1])) {
                            funcionesXmlDescarga.HistorialDescargas(opcion[2], opcion[3], "des", opcion[1]);
                        }

                        System.out.println("\nCantidad de descargas: "
                                + funcionesXmlDescarga.PersonasDescargando() + "\n");

                        System.out.println("Descarga del usuario: " + opcion[2] + ", Puerto: " + opcion[3] + " del video: " + opcion[1] + ":" + opcion[3]);
                        //ENVIA EL CLIENTE AL SERVIDOR DE MENOR DESCARGAS
                        FuncionCliente clientes = new FuncionCliente("localhost", 9003);
                        String res = clientes.EnviaRespuesta("3:" + opcion[1] + ":" + opcion[2] + ":" + opcion[3]);
                        funcionesXmlCola.Eliminar(opcion[2], opcion[3], opcion[1]);
                        Responder(res);
                    } else {
                        Responder("Servidores ocupados");
                    }
                    break;

                }
                default: {
                    throw new RuntimeException("ERROR EN LA OPERACION");
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(HiloCentral.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JDOMException ex) {
            Logger.getLogger(HiloCentral.class.getName()).log(Level.SEVERE, null, ex);
        }

        desconexion();
    }

}
