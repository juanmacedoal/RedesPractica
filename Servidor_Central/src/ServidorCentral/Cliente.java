/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServidorCentral;

/**
 *
 * @author Juan
 */
public class Cliente {

    private String nickname;
    private String password;
    private String ip;
    private int puerto;

    /**
     * Constructor
     *
     * @param nickname
     * @param password
     * @param ip
     * @param puerto
     */
    public Cliente(String nickname, String password, String ip, int puerto) {
        this.nickname = nickname;
        this.password = password;
        this.ip = ip;
        this.puerto = puerto;
    }

    /**
     * Get
     *
     * @return
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * Set
     *
     * @param nickname
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * Get
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Get
     *
     * @return
     */
    public String getIp() {
        return ip;
    }

    /**
     * Set
     *
     * @param ip
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * Get
     *
     * @return
     */
    public int getPuerto() {
        return puerto;
    }

    /**
     * Set
     *
     * @param puerto
     */
    public void setPuerto(int puerto) {
        this.puerto = puerto;
    }

}
