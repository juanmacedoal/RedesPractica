/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServidorCentral;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.JDOMParseException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import ServidorCentral.UtilXm2;

/**
 *
 * @author Luis
 */
public class FuncionesXml {

    /**
     * Recibe una lista de videos
     *
     * @param listVideo
     * @return
     */
    public static ListaVideos readAllObjectsFromFile2(ListaVideos listVideo) {

        Video Video = null;
        Document doc;
        Element root, child;
        List<Element> rootChildrens;
        String name, ruta, ip, puerto;
        int pos = 0;

        SAXBuilder builder = new SAXBuilder();

        try {
            doc = builder.build(UtilXm2.USERS_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (pos < rootChildrens.size()) {
                child = rootChildrens.get(pos);

                name = child.getAttributeValue(UtilXm2.USER_NOMBRE_TAG);
                ruta = child.getAttributeValue(UtilXm2.USER_RUTA_TAG);
                ip = child.getAttributeValue(UtilXm2.USER_IP_TAG);
                puerto = child.getAttributeValue(UtilXm2.USER_PUERTO_TAG);

                if (name != null && ruta != null && ip != null && puerto != null) {
                    Video = new Video(name, ip, Integer.parseInt(puerto), ruta);
                    listVideo.agregarVideo(Video);
                } else {

                    if (ip == null) {
                        System.out.println(UtilXml.ERROR_USER_NICKNAME_TAG);
                    }

                    if (puerto == null) {
                        System.out.println(UtilXml.ERROR_USER_PASSWORD_TAG);
                    }

                }

                pos++;
            }
            return listVideo;
        } catch (JDOMParseException e) {
            System.out.println(UtilXml.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        } catch (JDOMException e) {
            System.out.println(UtilXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println(UtilXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return listVideo;
    }

    /**
     * Guarda im archivo
     *
     * @param nombre
     * @param ruta
     * @param ip
     * @param puerto
     * @return
     * @throws JDOMException
     * @throws IOException
     */
    public static boolean saveObjectToFile(String nombre, String ruta, String ip, String puerto) throws JDOMException, IOException {
        Document doc;
        Element root, newChild, child, child2;
        List<Element> rootChildrens;
        int pos = 0;
        int cont = 0;
        SAXBuilder builder = new SAXBuilder();

        //System.out.println("Video "+nombre+" puerto salida "+puerto);
        doc = builder.build(UtilXm2.USERS_XML_PATH);
        root = doc.getRootElement();

        newChild = new Element(UtilXm2.SERVIDOR_TAG);
        newChild.setAttribute(UtilXm2.USER_NOMBRE_TAG, nombre);
        newChild.setAttribute(UtilXm2.USER_RUTA_TAG, ruta);
        newChild.setAttribute(UtilXm2.USER_IP_TAG, ip);
        newChild.setAttribute(UtilXm2.USER_PUERTO_TAG, puerto);
        root.addContent(newChild);
        try {
            Format format = Format.getPrettyFormat();

            //SALIDA DATO XML
            XMLOutputter out = new XMLOutputter(format);

            // GUARDA DATOS
            FileOutputStream file = new FileOutputStream(UtilXm2.USERS_XML_PATH);

            out.output(doc, file);

            file.flush();
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    /**
     * Eliminar un nodo del xml
     *
     * @throws JDOMException
     * @throws IOException
     */
    public static void Eliminar() throws JDOMException, IOException {
        //ELIMINAR REPETIDOS
        Document doc;
        Element root, newChild, child, child2;
        List<Element> rootChildrens;
        int pos = 0;
        int cont = 0;
        SAXBuilder builder = new SAXBuilder();
        doc = builder.build(UtilXm2.USERS_XML_PATH);
        root = doc.getRootElement();

        rootChildrens = root.getChildren();
        while (pos < rootChildrens.size()) {
            cont = rootChildrens.size() - 1;
            child = rootChildrens.get(pos);
            while (cont > 1) {
                child2 = rootChildrens.get(cont);
                if (child.getAttributeValue(UtilXm2.USER_NOMBRE_TAG).toString().equals(child2.getAttributeValue(UtilXm2.USER_NOMBRE_TAG).toString()) && cont > pos) {
                    root.removeContent(child2);
                }
                cont--;
            }
            pos++;
        }
        try {
            Format format = Format.getPrettyFormat();

            //SALIDA DATO XML
            XMLOutputter out = new XMLOutputter(format);

            // GUARDA DATOS
            FileOutputStream file = new FileOutputStream(UtilXm2.USERS_XML_PATH);

            out.output(doc, file);

            file.flush();
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
