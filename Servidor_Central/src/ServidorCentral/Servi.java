/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServidorCentral;

/**
 *
 * @author Juan   
 */
public class Servi {
    
     private String ip;
     private String puerto;
     private  int cantidad_cliente;
    /**
     * Recibe servidores
     * @param ip
     * @param puerto
     * @param cantidad_cliente 
     */
    public Servi(String ip, String puerto, int cantidad_cliente) {
        this.ip = ip;
        this.puerto = puerto;
        this.cantidad_cliente = cantidad_cliente;
    }
    /**
     * Recibe ip
     * @return 
     */
    public String getIp() {
        return ip;
    }
    /**
     * Asigna ip
     * @param ip 
     */
    public void setIp(String ip) {
        this.ip = ip;
    }
    /**
     * Recibe puerto
     * @return 
     */
    public String getPuerto() {
        return puerto;
    }
    /**
     * Asigna Puerto
     * @param puerto 
     */
    public void setPuerto(String puerto) {
        this.puerto = puerto;
    }
    /**
     * Get cantidad cliente
     * @return 
     */
    public int getCantidad_cliente() {
        return cantidad_cliente;
    }
    /**
     * Asigna cantidad
     * @param cantidad_cliente 
     */
    public void setCantidad_cliente(int cantidad_cliente) {
        this.cantidad_cliente = cantidad_cliente;
    }
     
     
     
    
}
