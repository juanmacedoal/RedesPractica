/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServidorCentral;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.JDOMParseException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author Juan
 */
public class funcionesXmlCola {

    /**
     * Cantidad de personas descargando
     *
     * @return
     * @throws JDOMException
     * @throws IOException
     */
    public static int PersonasDescargando() throws JDOMException, IOException {

        Video video = null;
        Document doc;
        Element root, child;
        List<Element> rootChildrens;
        String videos, ruta, ip, puerto, status;
        int pos = 0;
        int personas;
        SAXBuilder builder = new SAXBuilder();

        doc = builder.build(UtilXmlCol.USERS_XML_PATH);

        root = doc.getRootElement();

        rootChildrens = root.getChildren();
        personas = 0;
        while (pos < rootChildrens.size()) {
            child = rootChildrens.get(pos);
            videos = child.getAttributeValue(UtilXmlCol.USER_NOMBRE_TAG);
            ruta = child.getAttributeValue(UtilXmlCol.USER_RUTA_TAG);
            ip = child.getAttributeValue(UtilXmlCol.USER_IP_TAG);
            puerto = child.getAttributeValue(UtilXmlCol.USER_PUERTO_TAG);
            status = child.getAttributeValue(UtilXmlCol.USER_STATUS_TAG);

            if (videos != null && ip != null && puerto != null) {
                System.out.print("personas");
                personas++;
            } else {
                if (ip == null) {
                    System.out.println(UtilXml.ERROR_USER_NICKNAME_TAG);
                }

                if (puerto == null) {
                    System.out.println(UtilXml.ERROR_USER_PASSWORD_TAG);
                }
            }
            pos++;
        }
        return personas;
    }

    //BUSCA SI YA ESTA EN EL HISTORIAL
    public static boolean VerificarHistorial(String ips, String puertos, String sta, String videos) throws JDOMException, IOException {
        Document doc;
        Element root, child;
        List<Element> rootChildrens;
        String ip, puerto, status, tamano, ruta, video, porcentaje;
        boolean found = false;
        int pos = 0;
        SAXBuilder builder = new SAXBuilder();

        doc = (Document) builder.build(UtilXmlCol.USERS_XML_PATH);
        root = doc.getRootElement();
        rootChildrens = root.getChildren();
        while (!found && pos < rootChildrens.size()) {
            child = rootChildrens.get(pos);
            ip = child.getAttributeValue(UtilXmlCol.USER_IP_TAG);
            puerto = child.getAttributeValue(UtilXmlCol.USER_PUERTO_TAG);
            video = child.getAttributeValue(UtilXmlCol.USER_NOMBRE_TAG);
            status = child.getAttributeValue(UtilXmlCol.USER_STATUS_TAG);
            //System.out.println(" usuario"+ip+" usuario  "+puerto + puertos + ":" + video + videos + ":" + status);

            if (ip != null && puerto != null && puerto.equals(puertos) && videos.equals(video)) {
                System.out.println("\nYa se descargo por el usuario de ip = " + ip + ", el video Video " + video);
                return found = true;

            } else {
                if (ip == null) {
                    System.out.println(UtilXmlCol.ERROR_USER_IP_TAG);
                }

                if (puerto == null) {
                    System.out.println(UtilXmlCol.ERROR_USER_PUERTO_TAG);
                }

                pos++;
            }
        }
        return found;
    }

    /**
     * Historial de descargas
     *
     * @param ips
     * @param puertos
     * @param sta
     * @param videos
     * @return
     * @throws JDOMException
     * @throws IOException
     */
    public static boolean HistorialDescargas(String ips, String puertos, String sta, String videos) throws JDOMException, IOException {
        Document doc;
        Element root, newChild;

        SAXBuilder builder = new SAXBuilder();

        //System.out.println("usuario"+ips+" usuario  "+puertos);
        doc = builder.build(UtilXmlCol.USERS_XML_PATH);
        root = doc.getRootElement();
        newChild = new Element(UtilXmlCol.SERVIDOR_TAG);
        newChild.setAttribute(UtilXmlCol.USER_IP_TAG, ips);
        newChild.setAttribute(UtilXmlCol.USER_PUERTO_TAG, puertos);
        newChild.setAttribute(UtilXmlCol.USER_NOMBRE_TAG, videos);

        root.addContent(newChild);
        try {
            Format format = Format.getPrettyFormat();

            /* Se genera un flujo de salida de datos XML */
            XMLOutputter out = new XMLOutputter(format);

            /* Se asocia el flujo de salida con el archivo donde se guardaran los datos */
            FileOutputStream file = new FileOutputStream(UtilXmlCol.USERS_XML_PATH);

            /* Se manda el documento generado hacia el archivo XML */
            out.output(doc, file);

            /* Se limpia el buffer ocupado por el objeto file y se manda a cerrar el archivo */
            file.flush();
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    /**
     * Elimina los que descargaron
     *
     * @param usuario
     * @param puertoU
     * @param videos
     * @throws JDOMException
     * @throws IOException
     */
    public static void Eliminar(String usuario, String puertoU, String videos) throws JDOMException, IOException {
        //ELIMINAR REPETIDOS
        Document doc;
        Element root, newChild, child, child2;
        List<Element> rootChildrens;
        int pos = 0;
        int cont = 0;
        SAXBuilder builder = new SAXBuilder();
        doc = builder.build(UtilXmlCol.USERS_XML_PATH);
        root = doc.getRootElement();

        rootChildrens = root.getChildren();
        while (pos < rootChildrens.size()) {
            cont = rootChildrens.size() - 1;
            child = rootChildrens.get(pos);
            while (cont > 1) {
                child2 = rootChildrens.get(cont);
                if (child.getAttributeValue(UtilXmlCol.USER_NOMBRE_TAG).toString().equals(videos) && child.getAttributeValue(UtilXmlCol.USER_PUERTO_TAG).toString().equals(puertoU) && child.getAttributeValue(UtilXmlCol.USER_IP_TAG).toString().equals(usuario) && cont > pos) {
                    System.out.print(child.getAttributeValue("Borrar deberia " + UtilXmlCol.USER_NOMBRE_TAG));
                    root.removeContent(child2);
                }
                cont--;
            }
            pos++;
        }
        try {
            Format format = Format.getPrettyFormat();

            //SALIDA DATO XML
            XMLOutputter out = new XMLOutputter(format);

            // GUARDA DATOS
            FileOutputStream file = new FileOutputStream(UtilXmlCol.USERS_XML_PATH);

            out.output(doc, file);

            file.flush();
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
