/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServidorCentral;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Juan
 */
public class ListaServidor {

    private List<Servi> listaServidor;

    /**
     * Crea una lista
     */
    public ListaServidor() {
        this.listaServidor = new ArrayList();
    }

    /**
     * Agrega lista de servidores
     *
     * @param servidor
     */
    public void AgregarLista(Servi servidor) {
        this.listaServidor.add(servidor);
    }

    /**
     * Devuelve servidor
     *
     * @param cont
     * @return
     */
    public Servi GetServidor(int cont) {
        return this.listaServidor.get(cont);
    }

    /**
     * Tamaño de lista
     *
     * @return
     */
    public int size() {
        return this.listaServidor.size();
    }

}
