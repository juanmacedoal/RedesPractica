/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServidorCentral;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Luis
 */
public class ListaVideos {
    
    private List <Video> listaVideo;

    /**
     * Crea lista de videos
     */
    public ListaVideos() {
        this.listaVideo = new ArrayList();
    }
    /**
     * Agrega video a lista
     * @param video 
     */
    public void agregarVideo(Video video )
    {
        this.listaVideo.add(video);
    }
    /**
     * Busca video en lista
     * @param cont
     * @return 
     */
    public Video buscarvideo(int cont)
    {
        return this.listaVideo.get(cont);
    }
    /**
     * Devuelve tamaño
     * @return 
     */
    public int tamano_video()
    {
        return this.listaVideo.size();
    }
    
}
