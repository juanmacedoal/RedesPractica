/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServidorCentral;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Macedo
 */
public class ListDescargas {

    private List<Descargas> listaDescargas;

    /**
     * Crea lista descargas
     */
    public ListDescargas() {
        this.listaDescargas = new ArrayList();
    }

    /**
     * Agrega lista descarga
     *
     * @param descarga
     */
    public void agregarDescargas(Descargas descarga) {
        this.listaDescargas.add(descarga);
    }

    /**
     * Envia una lista
     *
     * @param cont
     * @return
     */
    public Descargas buscardescarga(int cont) {
        return this.listaDescargas.get(cont);
    }

    /**
     * Devuelve el tamaño de la lista
     *
     * @return
     */
    public int tamano_descarga() {
        return this.listaDescargas.size();
    }

}
