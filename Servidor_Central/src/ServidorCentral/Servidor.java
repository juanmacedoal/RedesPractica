/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServidorCentral;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Juan
 */
public class Servidor extends Thread {

    private ServerSocket ss;
    private int puerto;
    boolean parrar;

    /**
     * Constructor puerto escucha
     *
     * @param puerto_escucha
     */
    public Servidor(int puerto_escucha) {
        this.puerto = puerto_escucha;

    }

    /**
     * Cierra el hilo
     */
    @Override
    public void interrupt() {

        System.out.println("Entro para cerrar hilo");
        parrar = false;

    }

    /**
     * Corre el hilo
     */
    @Override
    public void run() {
        try {

            System.out.println("INICIANDO");
            ss = new ServerSocket(this.puerto);
            System.out.println("\t[OK]");
            int idSession = 0;
            parrar = true;

            while (parrar) {
                Socket socket;
                socket = ss.accept();
                System.out.println("NUEVA CONEXION: " + socket);
                ((HiloCentral) new HiloCentral(socket, idSession)).start();
                idSession++;
            }

        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
