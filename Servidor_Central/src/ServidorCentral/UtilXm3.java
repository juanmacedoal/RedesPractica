/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServidorCentral;

import static ServidorCentral.UtilXml.USER_PASSWORD_TAG;

/**
 *
 * @author Juan
 */
public class UtilXm3 {

    public static final String SERVIDOR_TAG = "servidor";
    public static final String USER_IP_TAG = "ip";
    public static final String USER_PUERTO_TAG = "puerto";

    public static final String ERROR_USER_IP_TAG = "Error loading mail from XML - Error in the attribute " + USER_IP_TAG + " of the XML tag";
    public static final String ERROR_USER_PUERTO_TAG = "Error loading image from XML - Error in the attribute " + USER_PUERTO_TAG + " of the XML tag";

    public static final String ERROR_XML_EMPTY_FILE = "Error loading XML file - The file is empty";
    public static final String ERROR_XML_PROCESSING_FILE = "Error loading XML file - It's not possible processing the file";
    public static final String ERROR_XML_PROFESSOR_ID_NOT_EXIST = "Error loading XML file - The professor don't exist";

    public static final String USERS_XML_PATH = "C:\\Users\\Macedo\\Desktop\\ultimo\\Redes\\Servidor_Central\\src\\ServidorCentral\\servidores_descargas.xml";

}
