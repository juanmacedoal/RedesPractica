/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servidordescarga;

import java.io.*;
import java.io.Serializable;
import java.net.Socket;
import java.net.UnknownHostException;
import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.*;
import java.util.logging.*;
import javax.swing.*;
import java.io.*;
import java.net.*;
import java.util.logging.*;

/**
 *
 * @author Juan
 */
public class FuncionCliente implements Serializable {

    private Socket s;
    private DataOutputStream p;
    private DataInputStream b;
    private ObjectOutputStream salida;
    private ObjectInputStream entrada;

    /**
     * Recibe puerto y abre conexion
     *
     * @param host
     * @param puerto
     */
    public FuncionCliente(String host, int puerto) {
        try {
            System.out.println("\nIP Servidor central: " + host);
            System.out.println("Puerto Servidor central: " + puerto);

            s = new Socket(host, puerto);

            //LECTURA Y ESCRITURA
            p = new DataOutputStream(s.getOutputStream());
            b = new DataInputStream(s.getInputStream());
        } catch (UnknownHostException e) {
            System.out.println("NO SE CONECTA " + host + ":" + puerto);
        } catch (IOException e) {
            System.out.println("ERROR " + host + ":" + puerto);
        }
    }

    /**
     * Desconexion
     */
    public void Desconectar() {
        try {
            p.close();
            b.close();
            s.close();
        } catch (UnknownHostException e) {
            System.out.println("NO SE CONECTA ");
        } catch (IOException e) {
            System.out.println("ERROR ");
        }
    }

    /**
     * Envia mensaje
     *
     * @param mensaje
     * @return
     */
    public String Enviar_mensaje(String mensaje) {

        String respuesta = "";

        try {

            p.writeUTF(mensaje);
            respuesta = b.readUTF();
            Desconectar();

            return respuesta;
        } catch (UnknownHostException e) {
            System.out.println("NO SE CONECTA");
        } catch (IOException e) {
            System.out.println("ERROR ");
        }

        return respuesta;
    }

}
