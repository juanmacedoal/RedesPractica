/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidordescarga;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.JDOMParseException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import servidordescarga.UtilXm2;

/**
 *
 * @author Juan
 */
public class VideoFuncionesXml {

    /**
     * Lee objetos xml
     *
     * @param listVideo
     * @return
     * @throws JDOMException
     * @throws IOException
     */
    public static Videos readAllObjectsFromFile2(Videos listVideo) throws JDOMException, IOException {
        Video video = null;
        Document doc;
        Element root, child;
        List<Element> rootChildrens;
        String name, ruta, ip, puerto;
        int pos = 0;

        SAXBuilder builder = new SAXBuilder();
        doc = builder.build(UtilXm2.USERS_XML_PATH);
        root = doc.getRootElement();
        rootChildrens = root.getChildren();

        while (pos < rootChildrens.size()) {
            child = rootChildrens.get(pos);

            name = child.getAttributeValue(UtilXm2.USER_NOMBRE_TAG);
            ruta = child.getAttributeValue(UtilXm2.USER_RUTA_TAG);
            ip = child.getAttributeValue(UtilXm2.USER_IP_TAG);
            puerto = child.getAttributeValue(UtilXm2.USER_PUERTO_TAG);

            System.out.println(name + " " + ruta + " " + ip + " " + puerto);
            if (name != null && ruta != null && ip != null && puerto != null) {
                System.out.println("dentro " + name + " " + ruta + " " + ip + " " + puerto);
                video = new Video(name, ip, Integer.parseInt(puerto), ruta);
                listVideo.agregarVideo(video);
            } else {
                if (ip == null) {
                    System.out.println(UtilXml.ERROR_USER_NICKNAME_TAG);
                }

                if (puerto == null) {
                    System.out.println(UtilXml.ERROR_USER_PASSWORD_TAG);
                }
            }

            pos++;
        }
        return listVideo;

    }
    
     /**
     * Verifica si tiene el video
     * @param ips
     * @param puertos
     * @param sta
     * @param videos
     * @return
     * @throws JDOMException
     * @throws IOException 
     */
    public static boolean VerificarHistorial(String ips, String puertos, String sta, String videos) throws JDOMException, IOException {
        Document doc;
        Element root, child;
        List<Element> rootChildrens;
        String ip, puerto, status, tamano, ruta, video, porcentaje;
        boolean found = false;
        int pos = 0;
        SAXBuilder builder = new SAXBuilder();

        doc = (Document) builder.build(UtilXm2.USERS_XML_PATH);
        root = doc.getRootElement();
        rootChildrens = root.getChildren();
        while (!found && pos < rootChildrens.size()) {
            child = rootChildrens.get(pos);
            ip = child.getAttributeValue(UtilXm2.USER_IP_TAG);
            puerto = child.getAttributeValue(UtilXm2.USER_PUERTO_TAG);
            video = child.getAttributeValue(UtilXm2.USER_NOMBRE_TAG);
            
            //System.out.println(" usuario" + ip + " usuario  " + puerto + puertos + ":" + video + videos);

            if (ip != null && puerto != null && video.equals(videos + ".mp4")) {
                System.out.println("\nSi existe en el servidor 3 de ip = " + ip + ", el video Video " + video);
                return found = true;

            } else {
                if (ip == null) {
                    System.out.println(UtilXml3.ERROR_USER_IP_TAG);
                }

                if (puerto == null) {
                    System.out.println(UtilXml3.ERROR_USER_PUERTO_TAG);
                }

                pos++;
            }
        }
        return found;
    }

}
