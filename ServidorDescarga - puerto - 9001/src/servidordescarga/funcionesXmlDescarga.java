/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidordescarga;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.JDOMParseException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author Juan
 */
public class funcionesXmlDescarga {

    /**
     * Cantidad de personas descargando
     *
     * @return
     * @throws JDOMException
     * @throws IOException
     */
    public static int PersonasDescargando() throws JDOMException, IOException {

        Video video = null;
        Document doc;
        Element root, child;
        List<Element> rootChildrens;
        String name, ruta, ip, puerto, status;
        int pos = 0;
        int personas;
        SAXBuilder builder = new SAXBuilder();

        doc = builder.build(UtilXml3.USERS_XML_PATH);

        root = doc.getRootElement();

        rootChildrens = root.getChildren();
        personas = 0;
        while (pos < rootChildrens.size()) {
            child = rootChildrens.get(pos);

            name = child.getAttributeValue(UtilXml3.USER_NOMBRE_TAG);
            ruta = child.getAttributeValue(UtilXml3.USER_RUTA_TAG);
            ip = child.getAttributeValue(UtilXml3.USER_IP_TAG);
            puerto = child.getAttributeValue(UtilXml3.USER_PUERTO_TAG);
            status = child.getAttributeValue(UtilXml3.USER_STATUS_TAG);

            if (name != null && ruta != null && ip != null && puerto != null && status.equals(" des")) {
                personas++;
            } else {
                if (ip == null) {
                    System.out.println(UtilXml.ERROR_USER_NICKNAME_TAG);
                }

                if (puerto == null) {
                    System.out.println(UtilXml.ERROR_USER_PASSWORD_TAG);
                }
            }
            pos++;
        }
        return personas;
    }

    //BUSCA SI YA ESTA EN EL HISTORIAL
    public static boolean VerificarHistorial(String ips, String puertos, String sta, String videos) throws JDOMException, IOException {
        Document doc;
        Element root, child;
        List<Element> rootChildrens;
        String ip, puerto, status, tamano, ruta, video, porcentaje;
        boolean found = false;
        int pos = 0;
        SAXBuilder builder = new SAXBuilder();

        doc = (Document) builder.build(UtilXml3.USERS_XML_PATH);
        root = doc.getRootElement();
        rootChildrens = root.getChildren();
        while (!found && pos < rootChildrens.size()) {
            child = rootChildrens.get(pos);
            ip = child.getAttributeValue(UtilXml3.USER_IP_TAG);
            puerto = child.getAttributeValue(UtilXml3.USER_PUERTO_TAG);
            video = child.getAttributeValue(UtilXml3.USER_NOMBRE_TAG);
            status = child.getAttributeValue(UtilXml3.USER_STATUS_TAG);
            //System.out.println(" usuario"+ip+" usuario  "+puerto + puertos + ":" + video + videos + ":" + status);

            if (ip != null && puerto != null && puerto.equals(puertos) && videos.equals(video)) {
                System.out.println("\nSi existe en el servidor 1 de ip = " + ip + ", el video Video " + video);
                return found = true;

            } else {
                if (ip == null) {
                    System.out.println(UtilXml3.ERROR_USER_IP_TAG);
                }

                if (puerto == null) {
                    System.out.println(UtilXml3.ERROR_USER_PUERTO_TAG);
                }

                pos++;
            }
        }
        return found;
    }

    /**
     * Lee objetos xml
     *
     * @param puertos
     * @param sta
     * @param videos
     * @param porce
     * @throws JDOMException
     * @throws IOException
     */
    public static void readObjectFromFile3(String puertos, String sta, String videos, String porce) throws JDOMException, IOException {

        Document doc;
        Element root, child, newchild;
        List<Element> rootChildrens;
        String nickName, password, ip, puerto, status, tamano, ruta, video, porcentaje;
        boolean found = false;
        int pos = 0;
        boolean save = false;

        SAXBuilder builder = new SAXBuilder();

        try {
            doc = builder.build(UtilXml3.USERS_XML_PATH);
            root = doc.getRootElement();
            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size()) {
                child = rootChildrens.get(pos);

                ip = child.getAttributeValue(UtilXml3.USER_IP_TAG);
                puerto = child.getAttributeValue(UtilXml3.USER_PUERTO_TAG);
                video = child.getAttributeValue(UtilXml3.USER_NOMBRE_TAG);
                status = child.getAttributeValue(UtilXml3.USER_STATUS_TAG);
                tamano = child.getAttributeValue(UtilXml3.USER_TAMANO_TAG);
                porcentaje = child.getAttributeValue(UtilXml3.USER_PORCENTAJE_TAG);
                System.out.println("  usuario" + ip + " usuario  " + puerto + "puerto" + puertos + "video" + videos + "video" + video + "status" + status + "est" + sta + "porce " + porce);
                if (ip != null && puerto != null && puerto.equals(puertos) && puerto.equals(puertos) && videos.equals(video) && status.equals(sta)) {
                    child.setAttribute(UtilXml3.USER_PORCENTAJE_TAG, porce);
                    found = true;
                } else {
                    if (ip == null) {
                        System.out.println(UtilXml3.ERROR_USER_IP_TAG);
                    }

                    if (puerto == null) {
                        System.out.println(UtilXml3.ERROR_USER_PUERTO_TAG);
                    }

                    pos++;
                }
            }
            try {
                Format format = Format.getPrettyFormat();
                XMLOutputter out = new XMLOutputter(format);
                FileOutputStream file = new FileOutputStream(UtilXml3.USERS_XML_PATH);
                out.output(doc, file);
                file.flush();
                file.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (JDOMParseException e) {
            System.out.println(UtilXml.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        } catch (JDOMException e) {
            System.out.println(UtilXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println(UtilXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

    }

    /**
     * Historial de descargas
     *
     * @param ips
     * @param puertos
     * @param sta
     * @param videos
     * @param porce
     * @param tamo
     * @param rut
     * @return
     * @throws JDOMException
     * @throws IOException
     */
    public static boolean HistorialDescargas(String ips, String puertos, String sta, String videos, String porce, String tamo, String rut) throws JDOMException, IOException {
        Document doc;
        Element root, newChild;

        SAXBuilder builder = new SAXBuilder();

        //System.out.println("usuario"+ips+" usuario  "+puertos);
        doc = builder.build(UtilXml3.USERS_XML_PATH);
        root = doc.getRootElement();
        newChild = new Element(UtilXml3.SERVIDOR_TAG);
        newChild.setAttribute(UtilXml3.USER_IP_TAG, ips);
        newChild.setAttribute(UtilXml3.USER_PUERTO_TAG, puertos);
        newChild.setAttribute(UtilXml3.USER_NOMBRE_TAG, videos);
        newChild.setAttribute(UtilXml3.USER_PORCENTAJE_TAG, porce);
        newChild.setAttribute(UtilXml3.USER_STATUS_TAG, sta);
        newChild.setAttribute(UtilXml3.USER_RUTA_TAG, rut);
        newChild.setAttribute(UtilXml3.USER_TAMANO_TAG, tamo);
        root.addContent(newChild);
        try {
            Format format = Format.getPrettyFormat();

            /* Se genera un flujo de salida de datos XML */
            XMLOutputter out = new XMLOutputter(format);

            /* Se asocia el flujo de salida con el archivo donde se guardaran los datos */
            FileOutputStream file = new FileOutputStream(UtilXml3.USERS_XML_PATH);

            /* Se manda el documento generado hacia el archivo XML */
            out.output(doc, file);

            /* Se limpia el buffer ocupado por el objeto file y se manda a cerrar el archivo */
            file.flush();
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    /**
     * Verifica estatus
     *
     * @param ips
     * @param puertos
     * @param sta
     * @param videos
     * @param porce
     * @return
     * @throws JDOMException
     * @throws IOException
     */
    public static boolean verificarStatus(String ips, String puertos, String sta, String videos, String porce) throws JDOMException, IOException {
        Document doc;
        Element root, child;
        List<Element> rootChildrens;
        String ip, puerto, status, tamano, ruta, video, porcentaje;
        boolean found = false;
        int pos = 0;
        SAXBuilder builder = new SAXBuilder();

        doc = (Document) builder.build(UtilXml3.USERS_XML_PATH);
        root = doc.getRootElement();
        rootChildrens = root.getChildren();
        while (!found && pos < rootChildrens.size()) {
            child = rootChildrens.get(pos);
            ip = child.getAttributeValue(UtilXml3.USER_IP_TAG);
            puerto = child.getAttributeValue(UtilXml3.USER_PUERTO_TAG);
            video = child.getAttributeValue(UtilXml3.USER_NOMBRE_TAG);
            status = child.getAttributeValue(UtilXml3.USER_STATUS_TAG);
            porcentaje = child.getAttributeValue(UtilXml3.USER_PORCENTAJE_TAG);
            tamano = child.getAttributeValue(UtilXml3.USER_TAMANO_TAG);
            System.out.println("verificar   usuario" + ip + " usuario out " + puerto + "ppuerto" + puertos + "video" + video + "videos" + videos + "status" + status + "sta" + sta + "tamano" + tamano + "porce" + porce);

            if (ip != null && puerto != null && puerto.equals(puertos) && videos.equals(video) && status.equals(sta) && tamano.equals(porce)) {
                found = true;
            } else {

                if (ip == null) {
                    System.out.println(UtilXml3.ERROR_USER_IP_TAG);
                }

                if (puerto == null) {
                    System.out.println(UtilXml3.ERROR_USER_PUERTO_TAG);
                }

                pos++;
            }

        }

        return found;
    }

    /**
     * Lista de historial
     *
     * @param listDescarga
     * @return
     */
    public static ListDescargas LeerHistorial(ListDescargas listDescarga) {

        Descargas Descarga = null;
        Document doc;
        Element root, child;
        List<Element> rootChildrens;
        String name, ruta, ip, puerto, tamaño;
        int pos = 0;

        SAXBuilder builder = new SAXBuilder();

        try {
            doc = builder.build(UtilXml3.USERS_XML_PATH);

            root = doc.getRootElement();

            rootChildrens = root.getChildren();

            while (pos < rootChildrens.size()) {
                child = rootChildrens.get(pos);

                name = child.getAttributeValue(UtilXml3.USER_NOMBRE_TAG);
                ruta = child.getAttributeValue(UtilXml3.USER_RUTA_TAG);
                ip = child.getAttributeValue(UtilXml3.USER_IP_TAG);
                puerto = child.getAttributeValue(UtilXml3.USER_PUERTO_TAG);
                tamaño = child.getAttributeValue(UtilXml3.USER_TAMANO_TAG);

                //System.out.println(name+" "+ruta+" "+ip+" "+puerto);
                if (name != null && ruta != null && ip != null && puerto != null && tamaño != null) {

                    //System.out.println("dentro "+name+" "+ruta+" "+ip+" "+puerto+" "+tamaño);
                    Descarga = new Descargas(name, ip, Integer.parseInt(puerto), ruta, Integer.parseInt(tamaño));
                    listDescarga.agregarDescargas(Descarga);

                } else {

                    if (ip == null) {
                        System.out.println(UtilXml.ERROR_USER_NICKNAME_TAG);
                    }

                    if (puerto == null) {
                        System.out.println(UtilXml.ERROR_USER_PASSWORD_TAG);
                    }

                }

                pos++;
            }
            return listDescarga;
        } catch (JDOMParseException e) {
            System.out.println(UtilXml.ERROR_XML_EMPTY_FILE);
            e.printStackTrace();
        } catch (JDOMException e) {
            System.out.println(UtilXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println(UtilXml.ERROR_XML_PROCESSING_FILE);
            e.printStackTrace();
        }

        return listDescarga;
    }
}
