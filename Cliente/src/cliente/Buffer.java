/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

/**
 *
 * @author Macedo
 */
public class Buffer {
    
    public int tamaño;
    public byte[] buffer;

    
    public Buffer(int tamaño, byte[] buffer) {
        this.tamaño = tamaño;
        this.buffer = buffer;
    }
    
    public Buffer() {
     
    }

    public int getTamaño() {
        return tamaño;
    }

    public void setTamaño(int tamaño) {
        this.tamaño = tamaño;
    }

    public byte[] getBuffer() {
        return buffer;
    }

    public void setBuffer(byte[] buffer) {
        this.buffer = buffer;
    }
    
    
    
}
