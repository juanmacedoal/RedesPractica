/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

/**
 *
 * @author Luis
 */
public class DireccionCliente {
    
    //IP Y PUERTO DEL CLIENTE
    public static String ip_cliente = "localhost";
    public static int puerto_cliente = 9999;
    
    //IP Y PUERTO DEL SERVIDOR CENTRAL
    public static String ip_servidor_central="localhost";
    public static int puerto_centro = 8181;
    //IP Y PUERTO DEL SERVIDOR DE DESCARGA
    public static String ip_servidor_descarga="localhost";
    public static int puerto_descarga = 9494;
    
}
