/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Juan
 */
public class Servidor extends Thread {

    private ServerSocket serversock;
    private int puerto;
    boolean parada;

    /**
     * Recibe el puerto que escuchara
     *
     * @param puerto_escucha
     */
    public Servidor(int puerto_escucha) {
        this.puerto = puerto_escucha;

    }

    /**
     * Informa que entro
     */
    @Override
    public void interrupt() {

        System.out.println("Inicio");
        parada = false;

    }

    /**
     * Funcion que recibe conexiones sockets
     */
    @Override
    public void run() {
        try {

            System.out.println("INICIANDO");
            serversock = new ServerSocket(this.puerto);
            int idSession = 0;
            parada = true;

            while (parada) {
                Socket socket;
                socket = serversock.accept();
                System.out.println("Nueva conexion: " + socket);
                ((HiloCliente) new HiloCliente(socket, idSession)).start();
                idSession++;
            }

        } catch (IOException ex) {
            System.out.println("Ha Ocurrido Un Error En El Cliente");
        }
    }

}
