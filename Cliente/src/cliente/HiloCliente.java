/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

/**
 *
 * @author Luis
 */
import static cliente.Videos.select;
import java.io.*;
import java.net.*;
import java.util.logging.*;
import javax.swing.JOptionPane;

public class HiloCliente extends Thread {

    private Socket socket;
    private DataOutputStream sal;
    private DataInputStream dis;
    private int sesion;
    byte[] buffer = null, bufferRecibido;
    int tam = 0, i2;
    FileOutputStream fos;
    BufferedOutputStream out;
    BufferedInputStream in;
    Registry registry = new Registry();

    /**
     * Constructor que inicia la conexion socket
     *
     * @param socket
     * @param id
     */
    public HiloCliente(Socket socket, int id) {
        this.socket = socket;
        this.sesion = id;
        try {
            sal = new DataOutputStream(socket.getOutputStream());
            dis = new DataInputStream(socket.getInputStream());
        } catch (IOException ex) {
            Logger.getLogger(HiloCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Cerrar la conexion del socket
     */
    public void desconexion() {
        try {
            this.socket.close();
        } catch (IOException ex) {
            Logger.getLogger(HiloCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //RECIBIR PETICIONES AL IGUAL QUE EL CENTRAL
    public String recibir_datos() throws IOException {
        return dis.readUTF();
    }

    //ENVIAR LA RESPUESTA DE PETICIONES AL IGUAL QUE EL CENTRAL
    public void Responder(String dato) throws IOException {
        sal.writeUTF(dato);
    }

    /**
     * Funcion que recibe peticiones de las conexiones
     */
    @Override
    public void run() {
        String peticion = "";

        System.out.println("");
        try {

            peticion = recibir_datos();
            System.out.print("Peticion " + peticion);
            String[] opcion = peticion.toString().split(":");
            System.out.println(opcion[0]);

            int operacion = Integer.parseInt(opcion[0]);

            switch (operacion) {
                //RECIBE ARCHIVO DEL HILO DE DESCARGA CASE 3 
                case 1: {
                    if (opcion[2].equals("1")) {
                        System.out.println("Recibiendo Archivo parte 1: " + opcion[1]);
                    }
                    if (opcion[2].equals("2")) {
                        System.out.println("Recibiendo Archivo parte 2: " + opcion[1]);
                    }
                    if (opcion[2].equals("3")) {
                        System.out.println("Recibiendo Archivo parte 3: " + opcion[1]);
                    }
                    //GUARDAR ARCHIVO, UBICACION
                    //REVISA CUAL DE LAS PARTES ESTA RECIBIENDO
                    if (opcion[2].equals("1")) {
                        fos = new FileOutputStream(registry.getDireccion() + opcion[1] + "part1.zip");
                        out = new BufferedOutputStream(fos);
                        in = new BufferedInputStream(socket.getInputStream());
                        //LECTURA DE VIDEO  
                        tam = Integer.parseInt(dis.readUTF());
                        buffer = new byte[tam];

                        //OBTIENE VIDEO
                        System.out.println("Estatus de la Descarga");
                        for (int i = 0; i < buffer.length; i++) {
                            //ACA HACEMOS EL PORCENTAJE DE DESCARGA
                            buffer[i] = (byte) in.read();
                            System.out.println(i * 100 / buffer.length + "%");
                        }

                        System.out.println("100% \nDescarga finalizada parte 1 ");

                        out.write(buffer);
                        out.flush();
                        in.close();
                        out.close();
                    }
                    if (opcion[2].equals("2")) {
                        fos = new FileOutputStream(registry.getDireccion() + opcion[1] + "part2.zip");
                        out = new BufferedOutputStream(fos);
                        in = new BufferedInputStream(socket.getInputStream());
                        //LECTURA DE VIDEO  
                        tam = Integer.parseInt(dis.readUTF());
                        buffer = new byte[tam];

                        //OBTIENE VIDEO
                        System.out.println("Estatus de la Descarga");
                        for (int i = 0; i < buffer.length; i++) {
                            //ACA HACEMOS EL PORCENTAJE DE DESCARGA
                            buffer[i] = (byte) in.read();
                            System.out.println(i * 100 / buffer.length + "%");
                        }

                        System.out.println("100% \nDescarga finalizada parte 2 ");

                        out.write(buffer);
                        out.flush();
                        in.close();
                        out.close();
                    }
                    if (opcion[2].equals("3")) {
                        fos = new FileOutputStream(registry.getDireccion() + opcion[1] + "part3.zip");
                        out = new BufferedOutputStream(fos);
                        in = new BufferedInputStream(socket.getInputStream());
                        //LECTURA DE VIDEO  
                        tam = Integer.parseInt(dis.readUTF());
                        buffer = new byte[tam];

                        //OBTIENE VIDEO
                        System.out.println("Estatus de la Descarga");
                        for (int i = 0; i < buffer.length; i++) {
                            //ACA HACEMOS EL PORCENTAJE DE DESCARGA
                            buffer[i] = (byte) in.read();
                            System.out.println(i * 100 / buffer.length + "%");
                        }

                        System.out.println("100% \nDescarga finalizada parte 3 ");
                        out.write(buffer);
                        out.flush();
                        in.close();
                        out.close();
                    }

                    break;
                }
                default: {
                    throw new RuntimeException("ERROR");
                }
            }

        } catch (IOException ex) {

            //ADVERTENCIA CUANDO SE CAE EL SERVIDOR
            System.out.println("OCURRIO UN ERROR");
        }

        desconexion();
    }
}
