/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 *
 * @author Macedo
 */
public class Extract {

    public Extract() {
    }

    /**
     * @param args
     */
    public static boolean extr(String rutaZip, String carpetaDestino) {

        boolean flag = false;

        try {
            ZipInputStream entradaZip = new ZipInputStream(new FileInputStream(
                    rutaZip));

            ZipEntry archivoSuelto = null;

            while ((archivoSuelto = entradaZip.getNextEntry()) != null) {
                System.out.println("Archivo leido: " + archivoSuelto.getName());
                File nuevoArchivo = new File(carpetaDestino + File.separator + archivoSuelto.getName());
                FileOutputStream salida = new FileOutputStream(nuevoArchivo);

                byte[] buffer = new byte[1024];
                int leido;

                while ((leido = entradaZip.read(buffer, 0, buffer.length)) != -1) {
                    salida.write(buffer, 0, leido);
                }
                salida.flush();
                salida.close();
            }

            entradaZip.close();
            flag = true;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return flag;
    }
}
