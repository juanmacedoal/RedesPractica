/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;
import java.io.*;
import java.io.Serializable;
import java.net.Socket;
import java.net.UnknownHostException;
import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.*;
import java.util.logging.*;
import javax.swing.*;
import java.io.*;
import java.net.*;
import java.util.logging.*;


/**
 *
 * @author Luis
 */
public class FuncionCliente implements Serializable {
  
    private Socket Sock;
    private DataOutputStream out;
    private DataInputStream in ;
    private ObjectOutputStream salida;
    private ObjectInputStream entrada;
       
   public FuncionCliente(String host,int puerto)
   {
        try
            {
                System.out.println("host: "+host);
                System.out.println("puerto: "+puerto);
                
                
                 Sock = new Socket(host,puerto);
                //ESCRITURA Y LECTURA DEL SOCKET
                 
                 out =  new DataOutputStream(Sock.getOutputStream());
                 in = new  DataInputStream(Sock.getInputStream());
            }
        
         catch (UnknownHostException e)
            {
                System.out.println("NO CONECTO" + host + ":" + puerto);
            } 
         catch (IOException e)
            {
                System.out.println("ERROR " + host + ":" + puerto);
            }
   }
   
   
   public void Desconectar()
   {             
             try
            {
                 out.close();
                 in.close();
                 Sock.close();
            }
            catch (UnknownHostException e)
            {
                System.out.println("NO SE PUDO CONECTAR ");
            } 
            catch (IOException e)
            {
                System.out.println("ERROR " );
            }
   }
   

    public String Enviar_mensaje(String mensaje)
    {
            
            String respuesta = "";
        
            try
            {
               
                 out.writeUTF(mensaje);
                 respuesta=in.readUTF();
                 Desconectar();
                                
                 return respuesta; 
            }
            catch (UnknownHostException e)
            {
                System.out.println("NO SE PUDO CONECTAR ");
            } 
            catch (IOException e)
            {
                System.out.println("ERROR" );
            }
            
           return respuesta;
    }
    
    
    
}
